package api

import (
	"encoding/json"

	"github.com/gofiber/fiber/v2"
)

type DadJokeGet struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

func GetRandom(c *fiber.Ctx) error {
	var apiResp DadJokeGet
	getFromApi(&apiResp)
	response, err := json.Marshal(apiResp)
	if err != nil {
		panic(err)
	}
	return c.SendString(string(response))
}

func getFromApi(resp *DadJokeGet) {
	a := fiber.Get("https://icanhazdadjoke.com").Set("Accept", "application/json")
	if err := a.Parse(); err != nil {
		panic(err)
	}
	a.Struct(resp)
}
