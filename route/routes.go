package route

import (
	"dadJokesApi/api"

	"github.com/gofiber/fiber/v2"
)

func Route(app *fiber.App) {
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	app.Get("/api/random/get", api.GetRandom)
}
