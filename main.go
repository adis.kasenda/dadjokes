package main

import (
	"dadJokesApi/route"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()

	route.Route(app)

	app.Listen(":9000")
}
